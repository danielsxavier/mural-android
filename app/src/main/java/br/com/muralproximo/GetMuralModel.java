package br.com.muralproximo;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;
import br.com.muralproximo.br.com.muralproximo.vo.Comentario;
import br.com.muralproximo.br.com.muralproximo.vo.Midia;
import br.com.muralproximo.br.com.muralproximo.vo.Mural;

/**
 * Created by daniel on 30/08/17.
 */

public class GetMuralModel extends AsyncTask<String, Void, Void> {

    private MainActivity mainActivity;

    public GetMuralModel(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    protected Void doInBackground(String... params) {

        String url = "https://desenv.oniq.com.br/mural/api/getmural/" + params[0];
        final String onClick = params[1];

        HttpsTrustManager.allowAllSSL();
        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("onResponse", response.toString());
                try{
                    JSONObject mural = response.getJSONObject("Mural");
                    JSONArray midias = mural.getJSONArray("Midia");
                    JSONArray comentarios = mural.getJSONArray("Comentarios");

                    List<Midia> midiaList = new ArrayList<>();
                    List<Comentario> comentarioList = new ArrayList<>();

                    //montar a lista de midias do mural
                    for(int i = 0; i < midias.length(); i++){
                        JSONObject midiaObj = midias.getJSONObject(i);
                        Midia midia = new Midia(midiaObj.getInt("id"), midiaObj.getString("nome"), midiaObj.getString("valor"),
                                midiaObj.getString("tipo"), midiaObj.getInt("MuralId"));
                        midiaList.add(midia);
                    }
                    for(int i = 0; i < comentarios.length(); i++){
                        JSONObject comentarioObj = comentarios.getJSONObject(i);
                        Comentario comentario = new Comentario(comentarioObj.getInt("id"), comentarioObj.getString("nome"),
                                comentarioObj.getString("comentario"), comentarioObj.getInt("MuralId"));
                        comentarioList.add(comentario);
                    }
                    Mural muralObj = new Mural(mural.getInt("id"), mural.getString("uniqueId"), mural.getString("tipo"), mural.getString("nome"));
                    muralObj.setMidiaList(midiaList);
                    muralObj.setComentarioList(comentarioList);

                    if(mainActivity != null){
                        mainActivity.addValue(muralObj.getUniqueid() + "_" + muralObj.getNome());
                        mainActivity.getAdapter().notifyDataSetChanged();
                    }

                    if(onClick != null){
                        Intent intent = new Intent(mainActivity, MuralActivity.class);
                        intent.putExtra("mural", muralObj);
                        mainActivity.startActivity(intent);
                    }
                }catch(JSONException e){
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("onErrorResponse", error.toString());
            }
        });

        RequestHelper.getInstance().addToRequestQueue(jsonObjectRequest);
        return null;
    }
}
