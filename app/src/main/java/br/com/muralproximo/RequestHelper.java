package br.com.muralproximo;

import android.content.Context;
import android.text.TextUtils;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by daniel on 30/08/17.
 */

public class RequestHelper{

    public static final String TAG = RequestHelper.class.getSimpleName();

    private static RequestHelper mInstance;
    private RequestQueue mRequestQueue;
    private Context ApplicationContext;

    public RequestHelper(Context context) {
        mInstance = this;
        this.ApplicationContext = context;
    }

    public static synchronized RequestHelper getInstance(){
        return mInstance;
    }

    public RequestQueue getRequestQueue(){
        if(mRequestQueue == null){
            mRequestQueue = Volley.newRequestQueue(this.ApplicationContext);
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue (Request<T> req, String tag){
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue (Request<T> req){
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}
