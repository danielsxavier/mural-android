package br.com.muralproximo.br.com.muralproximo.vo;

import java.io.Serializable;

/**
 * Created by daniel on 30/08/17.
 */

public class Midia implements Serializable {

    private int id;
    private String nome;
    private String valor;
    private String tipo;
    private int muralId;

    public Midia(int id, String nome, String valor, String tipo, int muralId) {
        this.id = id;
        this.nome = nome;
        this.valor = valor;
        this.tipo = tipo;
        this.muralId = muralId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getMuralId() {
        return muralId;
    }

    public void setMuralId(int muralId) {
        this.muralId = muralId;
    }
}
