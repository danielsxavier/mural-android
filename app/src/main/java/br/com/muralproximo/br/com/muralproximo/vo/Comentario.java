package br.com.muralproximo.br.com.muralproximo.vo;

import java.io.Serializable;

/**
 * Created by daniel on 30/08/17.
 */

public class Comentario implements Serializable {

    private int id;
    private String nome;
    private String comentario;
    private int muralId;

    public Comentario(int id, String nome, String comentario, int muralId) {
        this.id = id;
        this.nome = nome;
        this.comentario = comentario;
        this.muralId = muralId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public int getMuralId() {
        return muralId;
    }

    public void setMuralId(int muralId) {
        this.muralId = muralId;
    }
}
