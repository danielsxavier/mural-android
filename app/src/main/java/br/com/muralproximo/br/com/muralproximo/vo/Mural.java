package br.com.muralproximo.br.com.muralproximo.vo;

import java.io.Serializable;
import java.util.List;

/**
 * Created by daniel on 30/08/17.
 */

public class Mural implements Serializable{

    private int id;
    private String uniqueid;
    private String tipo;
    private String nome;
    private List<Midia> midiaList;
    private List<Comentario> comentarioList;

    public Mural(int id, String uniqueid, String tipo, String nome) {
        this.id = id;
        this.uniqueid = uniqueid;
        this.tipo = tipo;
        this.nome = nome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUniqueid() {
        return uniqueid;
    }

    public void setUniqueid(String uniqueid) {
        this.uniqueid = uniqueid;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public List<Midia> getMidiaList() {
        return midiaList;
    }

    public void setMidiaList(List<Midia> midiaList) {
        this.midiaList = midiaList;
    }

    public List<Comentario> getComentarioList() {
        return comentarioList;
    }

    public void setComentarioList(List<Comentario> comentarioList) {
        this.comentarioList = comentarioList;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
