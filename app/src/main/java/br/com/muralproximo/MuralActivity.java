package br.com.muralproximo;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.StrictMode;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import java.net.URL;
import java.util.ArrayList;

import br.com.muralproximo.br.com.muralproximo.vo.Mural;

/**
 * Created by daniel on 30/08/17.
 */

public class MuralActivity extends Activity{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.mural);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Mural mural = (Mural) getIntent().getSerializableExtra("mural");

        TextView nome = (TextView) findViewById(R.id.nome);
        TextView tipo = (TextView) findViewById(R.id.tipo);
        ImageView imageView = (ImageView) findViewById(R.id.imageView);
    try{
        URL url = new URL(mural.getMidiaList().get(0).getValor());
        Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        imageView.setImageBitmap(bmp);
    }catch(Exception e){
        e.printStackTrace();
    }

        ArrayList<String> values = new ArrayList<>();
        for(int i = 0; i < mural.getComentarioList().size(); i++){
            values.add(mural.getComentarioList().get(i).getNome() + ": " + mural.getComentarioList().get(i).getComentario());
        }
        ListView listView = (ListView) findViewById(android.R.id.list);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, values);

        listView.setAdapter(adapter);

        nome.setText("Nome: " + mural.getNome());
        tipo.setText("Tipo: " + mural.getTipo());
    }
}
