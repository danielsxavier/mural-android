package br.com.muralproximo;

import android.Manifest;
import android.app.ListActivity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.kontakt.sdk.android.ble.connection.OnServiceReadyListener;
import com.kontakt.sdk.android.ble.manager.ProximityManager;
import com.kontakt.sdk.android.ble.manager.ProximityManagerFactory;
import com.kontakt.sdk.android.ble.manager.listeners.EddystoneListener;
import com.kontakt.sdk.android.ble.manager.listeners.IBeaconListener;
import com.kontakt.sdk.android.ble.manager.listeners.simple.SimpleEddystoneListener;
import com.kontakt.sdk.android.ble.manager.listeners.simple.SimpleIBeaconListener;
import com.kontakt.sdk.android.common.profile.IBeaconDevice;
import com.kontakt.sdk.android.common.profile.IBeaconRegion;
import com.kontakt.sdk.android.common.profile.IEddystoneDevice;
import com.kontakt.sdk.android.common.profile.IEddystoneNamespace;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends ListActivity {

    private ProximityManager proximityManager;
    private int permissionCheck;
    private List<String> values;
    private ArrayAdapter<String> adapter;
    private TextView textView;
    private MainActivity mainActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
            if(permissionCheck == PackageManager.PERMISSION_GRANTED){
                proximityManager = ProximityManagerFactory.create(this);
                proximityManager.setIBeaconListener(createIBeaconListener());
                proximityManager.setEddystoneListener(createEddystoneListener());
            }
        textView = (TextView) findViewById(R.id.naoexiste);
        ListView listView = (ListView) findViewById(android.R.id.list);
        values = new ArrayList<>();
        mainActivity  = this;
        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, values);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                GetMuralModel getMuralModel = new GetMuralModel(mainActivity);
                getMuralModel.execute(values.get(i).split("_")[0], "onClick");
            }
        });
    }

    @Override
    protected void onStart() {
        if(permissionCheck != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        else
            startScanning();

        super.onStart();
    }

    @Override
    protected void onStop() {
        if(permissionCheck == PackageManager.PERMISSION_GRANTED)
            proximityManager.stopScanning();

        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if(permissionCheck == PackageManager.PERMISSION_GRANTED) {
            proximityManager.disconnect();
            proximityManager = null;
        }
        super.onDestroy();
    }

    private void startScanning() {
        proximityManager.connect(new OnServiceReadyListener() {
            @Override
            public void onServiceReady() {
                proximityManager.startScanning();
            }
        });
    }

    private IBeaconListener createIBeaconListener() {
        return new SimpleIBeaconListener() {
            @Override
            public void onIBeaconDiscovered(IBeaconDevice ibeacon, IBeaconRegion region) {
                Log.i("Sample", "IBeacon discovered: " + ibeacon.toString());
                GetMuralModel getMuralModel = new GetMuralModel(mainActivity);
                getMuralModel.execute(ibeacon.getUniqueId(), null);
            }
        };
    }

    private EddystoneListener createEddystoneListener() {
        return new SimpleEddystoneListener() {
            @Override
            public void onEddystoneDiscovered(IEddystoneDevice eddystone, IEddystoneNamespace namespace) {
                Log.i("Sample", "Eddystone discovered: " + eddystone.toString());
                GetMuralModel getMuralModel = new GetMuralModel(mainActivity);
                getMuralModel.execute(eddystone.getUniqueId(), null);
            }
        };
    }

    public ArrayAdapter<String> getAdapter() {
        return adapter;
    }


    public synchronized void addValue(String value){
        if(!this.values.contains(value))
            this.values.add(value);
        textView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode){
            case 1: {
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    proximityManager = ProximityManagerFactory.create(this);
                    proximityManager.setIBeaconListener(createIBeaconListener());
                    proximityManager.setEddystoneListener(createEddystoneListener());
                    startScanning();
                }
            }
        }
    }


}