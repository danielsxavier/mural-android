package br.com.muralproximo;

import android.app.Application;
import com.kontakt.sdk.android.common.KontaktSDK;

/**
 * Created by daniel on 28/08/17.
 */

public class MuralProximoApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        KontaktSDK.initialize(this);
        RequestHelper requestHelperInstance = new RequestHelper(this);
    }
}
